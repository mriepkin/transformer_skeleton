from microservice_system_engine.abstract_classes import AbstractTransformer
from microservice_system_engine.utils import FeatureData
from transformers.features.br1_1 import Br1_1
from typing import List
import pandas as pd
from microservice_system_engine.utils import Alias


class Transformer(AbstractTransformer):
    __instance: 'Transformer' = None

    #TODO: add method for features addition
    def __init__(self):
        if not Transformer.__instance:
            super().__init__('BusinessRules', [Br1_1('BusinessRules')])
            Transformer.__instance = self

    @staticmethod
    def get_instance() -> 'Transformer':
        if Transformer.__instance is None:
            Transformer.__instance = Transformer()
        return Transformer.__instance