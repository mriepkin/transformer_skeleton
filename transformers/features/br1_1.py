from microservice_system_engine.abstract_classes import AbstractFeature, AbstractFeatureImplementation
from microservice_system_engine.utils import FeatureVersionAlias
import numpy as np
from typing import Dict


class Br1_1Implementation_1(AbstractFeatureImplementation):
    def __init__(self, transformer_name: str):
        super().__init__(transformer_name, 1)

    def execute(self, data, additional_data, aliases: Dict[str, FeatureVersionAlias]):
        tmp = []
        for row in data[[aliases['CustomerInfoFeatures_Age'].feature_alias]].values:
            tmp.append(0 if row[0] not in (None, np.nan) and row[0] > 17 else 1)
        data[aliases[self.transformer_name + '_BR1.1'].feature_alias] = tmp
        return data, [aliases[self.transformer_name + '_BR1.1'].feature_alias]


class Br1_1(AbstractFeature):

    def __init__(self, transformer_name: str):
        super(Br1_1, self).__init__('BR1.1', [Br1_1Implementation_1(transformer_name)])

